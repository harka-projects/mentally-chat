# Mentally

[The website](https://mentally.harka.com)

[The presentation](https://mentally.harka.com/assets/documents/mentally-presentation.pdf)

[The code](https://gitlab.com/mentally-project)

[The pitch](https://mentally.harka.com/assets/documents/mentally-pitch.pdf)

[The research](https://mentally.harka.com/assets/documents/mentally-research.pdf)

[The photos](https://drive.google.com/drive/folders/1TAzblUMlIqaQDwCqOVM2uTUww2hwMQ2b?usp=sharing)
